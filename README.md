## UWP Brush
这是一个用来展示UWP画笔的示例程序<br/>
程序中包含了:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;纯色画笔<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;线性渐变画笔<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平铺画笔<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;亚克力画笔<br/>
你可以切换和修改参数来自定义你的画笔
## 文档
CSDN: [https://blog.csdn.net/qq_39200794/article/details/120655158](https://blog.csdn.net/qq_39200794/article/details/120655158)
## 截图
![纯色画笔](https://bu.dusays.com/2021/10/08/c57ea70181984.png)
![线性渐变画笔](https://bu.dusays.com/2021/10/08/8b626981b5675.png)
![平铺画笔](https://bu.dusays.com/2021/10/08/ea800467b0454.png)
![亚克力画笔](https://bu.dusays.com/2021/10/08/d8206a301999e.png)
## 环境
程序使用 Visual Studio 2019 编译通过
## 克隆
```bash
git clone https://gitee.com/dearxuan/uwp-brush.git
```
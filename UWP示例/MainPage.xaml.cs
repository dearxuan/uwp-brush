﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;

// https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x804 上介绍了“空白页”项模板

namespace UWP示例
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            grid.Background = 纯色画笔();
        }

        public Brush 纯色画笔()
        {
            //定义Color
            Color color = new Color() { A = 255, R = 0, G = 0, B = 255 };
            //下列代码是等效的
            //Color color = Color.FromArgb(255, 0, 0, 255);
            SolidColorBrush brush = new SolidColorBrush(color);
            return brush;
        }

        public Brush 线性渐变画笔()
        {
            //定义线性渐变画笔
            LinearGradientBrush brush = new LinearGradientBrush();
            //定义梯度点
            GradientStop gradientStop1 = new GradientStop();
            GradientStop gradientStop2 = new GradientStop();
            GradientStop gradientStop3 = new GradientStop();
            //设置梯度点颜色和偏移
            gradientStop1.Color = Colors.Black;
            gradientStop1.Offset = 0;
            gradientStop2.Color = Colors.White;
            gradientStop2.Offset = 0.3;
            gradientStop3.Color = Colors.Red;
            gradientStop3.Offset = 1;
            //添加梯度点
            brush.GradientStops.Add(gradientStop1);
            brush.GradientStops.Add(gradientStop2);
            brush.GradientStops.Add(gradientStop3);
            //设置起始点和终点
            brush.StartPoint = new Point(0, 0);
            brush.EndPoint = new Point(1, 0);
            //定位格式
            //brush.MappingMode = BrushMappingMode.Absolute; // 绝对坐标
            //brush.MappingMode = BrushMappingMode.RelativeToBoundingBox; // 相对坐标
            return brush;
        }

        public Brush 平铺画笔()
        {
            //定义平铺画笔
            ImageBrush brush = new ImageBrush();
            //加载图片
            brush.ImageSource = new BitmapImage(new Uri("ms-appx:///Assets/1.jpg"));
            //设置图片在保留纵横比的同时适应控件大小
            brush.Stretch = Stretch.Uniform;
            return brush;
        }

        public Brush 亚克力画笔()
        {
            AcrylicBrush brush = new AcrylicBrush();
            brush.BackgroundSource = AcrylicBackgroundSource.HostBackdrop; // 从窗口后面采样
            //brush.BackgroundSource = AcrylicBackgroundSource.Backdrop; // 从控件中采样
            //不透明度
            brush.TintOpacity = 0;
            //颜色
            brush.TintColor = Colors.White;
            return brush;
        }

        private void onSelectChange(object sender, SelectionChangedEventArgs e)
        {
            switch (comboBox.SelectedIndex)
            {
                case 0:
                    grid.Background = 纯色画笔();
                    break;
                case 1:
                    grid.Background = 线性渐变画笔();
                    break;
                case 2:
                    grid.Background = 平铺画笔();
                    break;
                case 3:
                    grid.Background = 亚克力画笔();
                    break;
            }
        }
    }
}
